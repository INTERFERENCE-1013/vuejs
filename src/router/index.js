import Vue from 'vue'
import VueRouter from 'vue-router'

Vue.use(VueRouter)

  const routes = [
  {
    path: '/',
    name: 'Heroes',
    component: () => import(/* webpackChunkName: "superheroes" */ '../views/Heroes.vue')
  },
  {
    path: '/superheroes/:id',
    name: 'HeroInfo',
    component: () => import(/* webpackChunkName: "superheroes" */ '../views/HeroInfo.vue')
  }
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

export default router
